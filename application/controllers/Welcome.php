<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Tampil");
	}
	public function index()
	{
		$data = [
			'logo' => $this->Tampil->getdata('profil'),
			'curs' => $this->Tampil->getdata('valas')
		];
		$this->load->view('tampil',$data);
	}

	public function playlist(){
		//saya langsung ketik ya, gak dimasukin ke model, nanti mas aja yang pindahin
//kalo saya bikin di database nyimpannya  video1.mp4|video2.mp4|video3.mp4 bisa? (sesuai kebutuhan gak)
		// disatuin maskudnya. Tapi kalo semisal perlu judul atau data lain memang dipisah
		$this->db->select('*');
		$data=$this->db->get('vidio')->result_array();
		$i=0;
		$video=[];
		foreach($data as $value){
			$video[$i]=[
				'id'=>$value['id'],
				'nama'=>$value['nama'],
				'vidio'=>$value['vidio']
			];
			$i++;
		}
		echo json_encode($video);
	}
}
