-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2020 at 01:55 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curs`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `email`, `username`, `pass`, `gambar`) VALUES
(3, 'Admin@mail.com', 'Admin', 'lili', '1607473002.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `logo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `nama`, `logo`) VALUES
(1, 'logo', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `valas`
--

CREATE TABLE `valas` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `beli` int(10) NOT NULL,
  `jual` int(10) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `valas`
--

INSERT INTO `valas` (`id`, `nama`, `beli`, `jual`, `gambar`) VALUES
(5, 'lala', 1234, 2441, 'bandera-de-argentina-significado-de-los-colores-e-historia.png'),
(7, 'nana', 132, 123, 'Flag_of_the_United_States.svg'),
(8, 'wwr', 1223, 1323, 'Flag_of_the_United_States.svg'),
(9, 'dee', 12300, 12300, 'Flag_of_the_United_States.svg'),
(10, 'gghff', 1212, 1313, 'Flag_of_the_United_States.svg'),
(11, 'wewewe', 23434, 23432, 'Flag_of_the_United_States.svg'),
(12, 'rtf', 12345, 12344, 'Flag_of_the_United_States.svg'),
(13, 'fvf', 12000, 13000, 'Flag_of_the_United_States.svg'),
(14, 'fdf', 12345, 12345, 'Flag_of_the_United_States.svg');

-- --------------------------------------------------------

--
-- Table structure for table `vidio`
--

CREATE TABLE `vidio` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `vidio` varchar(800) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vidio`
--

INSERT INTO `vidio` (`id`, `nama`, `vidio`) VALUES
(19, 'nana', 'BLACKPINK – ‘Lovesick Girls’ M-V.webm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valas`
--
ALTER TABLE `valas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vidio`
--
ALTER TABLE `vidio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `valas`
--
ALTER TABLE `valas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `vidio`
--
ALTER TABLE `vidio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
